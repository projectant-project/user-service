import { Model } from 'mongoose';
import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interfaces/users.interface';
import { CreateDTO } from './dto/create.dto';
import { Role } from 'users/enums/role.enum';

import * as sha256 from 'hash.js/lib/hash/sha/256';
import { isNullOrUndefined } from 'util';

@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    // 1
    async findAll(): Promise<User[]> {
        return await this.userModel.find();
    }

    // 2
    async findByUsername(username: String): Promise<User> {
        return await this.userModel.findOne({username});
        // return this.userModel.findOne({ username: usernameValue }).select('password -_id').exec();
    }

    // 3
    async create(user: CreateDTO): Promise<User> {
        let foundUser: User = await this.findByUsername(user.username);
        
        if (!isNullOrUndefined(foundUser)) {
            console.log('User already exists');
            throw new BadRequestException('User already exists');
        }

        // user.password = this.encrypt(user.password); // -> for postman
        const createUser = new this.userModel(user);
        console.log('User created successfully');
        return createUser.save();
    }

    // 4
    async deleteByUsername(username: String) {
        try{
            return await this.userModel.deleteOne({username});
        }
        catch(error){
            console.log(error)
        }
    }

    // 5
    async deleteAllUsers() {
        try {
            return await this.userModel.deleteMany({});
        }
        catch(error) {
            console.log(error);
        }
    }

    // 6
    async updateUserPassword(username: String, newPassword: String) {
        try {
            return await this.userModel.updateOne({ username }, { password: newPassword });
        //    return await this.userModel.updateOne({ username }, { password: this.encrypt(newPassword) }); // -> for postman
        }
        catch(error) {
            console.log(error);
        }
    }

    // 7
    async updateManyUsersPassword(newPassword: String) {
        try {
            return await this.userModel.updateMany({}, { password: newPassword });
            // return await this.userModel.updateMany({}, { password: this.encrypt(newPassword) }); // -> for postman
        }
        catch(error) {
            console.log(error);
        }
    }

    // 8
    async login(username: String, password: String) {
        try {
            return await this.userModel.findOne({ username , password: password});
            // return await this.userModel.findOne({ username , password: this.encrypt(password)}); // -> for postman
        }
        catch(error) {
            console.log(error);
        }
    }

    encrypt(password: String): String {
        return sha256().update(password).digest('hex');
    }


};
