import { Controller, Get, Post, Delete, Put, Param, Query, Body, NotFoundException, BadRequestException, ForbiddenException } from '@nestjs/common';
import { CreateDTO } from 'users/dto/create.dto';
import { UsersService } from './users.service';
import { isNullOrUndefined } from 'util';
import { UpdateResponse } from './interfaces/update-response.interface';
import { resolve } from 'path';


@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) { }

    // 8 - login
    @Get('login')
    async login(@Query() query) {
        console.log('login');
        console.log(query.username, query.password);
        let u = await this.usersService.login(query.username, query.password);
        if (isNullOrUndefined(u)) {
            console.log('details are wrong');
            throw new ForbiddenException('Details are incorrect')
        }
        return u;
    }

    // 1 - get all users
    @Get()
    async getUsers() {
        console.log('get all users');
        let u = await this.usersService.findAll();
        if (u.length === 0) {
            throw new NotFoundException("No users found");
        }
        return u;
    }

    // 2 - get specific user by username
    @Get('/:username')
    async getUserByUsername(@Param('username') username) {
        console.log('get user by username');
        let u = await this.usersService.findByUsername(username);
        if (isNullOrUndefined(u)) {
            throw new NotFoundException("User not found");
        }
        return u;
    }

    // 3 - add user if username not found
    @Post()
    async createUser(@Body() userDto: CreateDTO) {
        console.log('create user');
        console.log(userDto.username, userDto.password);
        return this.usersService.create(userDto);
    }

    // 4 - delete user by username
    @Delete('/:username')
    async deleteByUsername(@Param('username') username) {
        console.log('delete user by username')
        let u = await this.usersService.deleteByUsername(username);
        if (isNullOrUndefined(u)) {
            console.log('User not found');
            throw new NotFoundException('User not found');
        }
        return u;
    }

    // 5 - delete all users
    // @Delete()
    // async deleteAllUsers() {
    //     console.log('delete all users')
    //     let u = await this.usersService.deleteAllUsers();
    //     if (u.n === 0) {
    //         console.log('User not found');
    //         throw new NotFoundException('User not found');
    //     }
    //     return u;
    // }
    @Delete()
    async deleteManyUsers(@Query() query) {
        console.log('delete all users')
        console.log(query);

        let ok = 0;

        let userArr: Promise<any>[] = [];
        for (const username of query.user) {
            userArr.push(this.usersService.deleteByUsername(username));
        }
        let a = await Promise.all(userArr);

        a.forEach((x) => {
            ok = (x.ok) ? 1 : ok;
        })

        console.log('status', ok);
        return ok;

        // let u = await this.usersService.deleteAllUsers();
        // if (u.n === 0) {
        //     console.log('User not found');
        //     throw new NotFoundException('User not found');
        // }
        // return u;
    }
    

    // 6 - update user password
    @Put('/:username')
    async updateUserPassword(@Param('username') username, @Body() body) {
        console.log('update user password');
        console.log(username, body.password);

        let u = await this.usersService.updateUserPassword(username, body.password);
        if (isNullOrUndefined(u)) {
            throw new NotFoundException('User not found');
        }
        return u;
    }

    // 7 - update all users password
    @Put()
    async updateManyUsersPassword(@Body() body) {
        console.log('update all users password');
        console.log(body);
        console.log(body.password);
        let answer: UpdateResponse = { n: 0, nModified: 0, ok: 0 };

        let taskArray: Promise<any>[] = [];
        for (const user of body.users) {
            taskArray.push(this.usersService.updateUserPassword(user.user.username, body.password));
        }
        let a = await Promise.all(taskArray);
        
        // sum all nModified in a

        a.forEach((x) => {
            answer.n += x.n;
            answer.nModified = x.nModified;
            answer.ok = (x.ok) ? 1 : answer.ok;
        })

        console.log('answer', answer);
        return answer;
    }

    /*
    // create
    // @Post('create')
    // createUser(@Query() query) {
    //     console.log('create');
    //     console.log(query.username, query.password, query.type);
    //     this.usersService.create(new userDTO(query.username, query.password, query.type));
    // }
    */



    // @Post()
    // createUser() {
    //     console.log('create');
    //     this.usersService.create(this.userDto);
    // }
}
