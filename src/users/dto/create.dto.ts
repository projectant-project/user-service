import { Role } from "users/enums/role.enum";

export class CreateDTO {
    public username: String;
    public password: String;
    public type: Role;
}