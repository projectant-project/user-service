import { Document } from 'mongoose';
import { Role } from 'users/enums/role.enum';

export interface User extends Document {
    username: string,
    password: string,
    type: Role
}